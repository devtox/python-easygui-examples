
### GUI Examples

EasyGUI user interfaction module example.
These examples use easy gui to get user input.

EasyGUI is a Python module that uses tkinter underneath.
More on GUI programming:
* https://pythonbasics.org
* https://pythonprogramminglanguage.com

<img src="easygui2.png">
<img src="easygui3.png">
<img src="easygui5.png">
<img src="easygui6.png">
<img src="easygui.png">
