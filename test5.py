import easygui as g

def integerbox(msg,title,minNum,maxNum):
    msg = g.integerbox(msg, title,default=0, lowerbound=minNum, upperbound=maxNum, image=None)
    print(msg)

integerbox('content', 'title',0,99)

